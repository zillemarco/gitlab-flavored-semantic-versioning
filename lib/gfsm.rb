# frozen_string_literal: true

# Load the data
require 'data/change_type'
require 'data/commit'
require 'data/configuration'
require 'data/version'

# Load the tools
require 'tools/commits_subdivider'
require 'tools/current_version_loader'
require 'tools/git_utilities'
require 'tools/output'
require 'tools/version_bumper'

# Load the commands
require 'commands/base_command'
require 'commands/changelog'
require 'commands/version'
require 'commands/help'

module GFSM
  # This is the map between each command and its implementation class
  COMMANDS = {
    'version' => -> { GFSM::Commands::Version },
    'changelog' => -> { GFSM::Commands::Changelog }
  }.freeze

  def self.main
    subcommand = ARGV.shift

    exit(COMMANDS[subcommand].call.new.run(ARGV)) if COMMANDS.key?(subcommand)

    case subcommand
    when /-{0,2}help/, '-h', nil
      GFSM::Commands::Help.new.run(ARGV)
    else
      GFSM::Output.warn "#{subcommand} is not a GFSM command."

      GFSM::Output.info "See 'gfsm help' for more detail."
      false
    end
  end
end
