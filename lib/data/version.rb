# frozen_string_literal: true

require 'sem_version'

module GFSM
  module Data
    class Version < SemVersion
      PRERELEASE_BUILD_VERSION = /^(?<prerelease_name>[a-zA-Z\-]+)(\.?)(?<prerelease_version>[0-9]*)$/i

      def bump!(major = false, minor = false, patch = true, pre = false, prerelease_name)
        if major
          self.major = self.major + 1
          self.minor = 0
          self.patch = 0
        elsif minor
          self.minor = self.minor + 1
          self.patch = 0
        elsif patch
          self.patch = self.patch + 1
        end

        if pre
          self.add_prerelease_suffix!(prerelease_name)
        end
      end

      def add_prerelease_suffix!(prerelease_name)
        preselease_match = self.pre.match(PRERELEASE_BUILD_VERSION) if self.pre
        if preselease_match
          self.pre = preselease_match[:prerelease_name] + "." + (preselease_match[:prerelease_version].to_i + 1).to_s
        else
          self.pre = prerelease_name
        end
      end
    end
  end
end