# frozen_string_literal: true

require 'git'

module GFSM
  module Tools
    class GitUtilities
      CHANGELOG_TRAILER_REGEX = /^(?<name>Changelog):\s*(?<category>.+)$/i

      def self.load_repo(repo_path)
        Git.open(repo_path)
      end

      def self.extract_last_tag_name(repo)
        begin
          repo.fetch(nil, {tags: true})
          repo.describe(nil, {tags: true, abbrev: 0})
        rescue
        end
      end

      def self.extract_commits_with_changelog_trailer(repo, from, to = "HEAD")
        begin
          commits = from.nil? ? repo.log : repo.log.between(from, to)
          commits.each_with_object([]) do |commit, memo|
            trailer = commit.message.match(CHANGELOG_TRAILER_REGEX)
            memo << GFSM::Data::Commit.new(commit, trailer[:name], trailer[:category]) if trailer
          end
        rescue
          []
        end
      end
    end
  end
end