# frozen_stirng_literal: true

module GFSM
  module Output
    def self.included(klass)
      klass.extend(Methods)
    end

    module Methods
      def stdout_handle
        $stdout.tap { |handle| handle.sync = true }
      end

      def stderr_handle
        $stderr.tap { |handle| handle.sync = true }
      end

      def print(message = nil, stderr: false)
        stderr ? stderr_handle.print(message) : stdout_handle.print(message)
      end

      def puts(message = nil, stderr: false)
        stderr ? stderr_handle.puts(message) : stdout_handle.puts(message)
      end

      def info(message)
        puts(message)
      end

      def warn(message)
        puts("WARNING: #{message}", stderr: true)
      end

      def error(message)
        puts("ERROR: #{message}", stderr: true)
      end

      def success(message)
        puts(message)
      end
    end

    extend Methods
    include Methods
  end
end