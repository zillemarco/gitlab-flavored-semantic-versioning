# frozen_string_literal: true

require 'git'

module GFSM
  module Tools
    class CommitsSubdivider
      def self.subdivide_commits(configuration, commits)
        subdivisions = {}

        commits.each do |commit|
          change_type = configuration.get_change_type_from_category(commit.category)
          
          if change_type
            (subdivisions[change_type] ||= []) << commit
          end
        end

        return subdivisions
      end
    end
  end
end