# frozen_string_literal: true

module GFSM
  module Commands
    class BaseCommand
      attr_reader :stdout, :stderr

      def initialize(stdout: GFSM::Output, stderr: GFSM::Output)
        @stdout = stdout
        @stderr = stderr
      end

      def run(args = [])
        raise NotImplementedError
      end
    end
  end
end
