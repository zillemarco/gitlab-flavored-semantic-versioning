Gem::Specification.new do |spec|
  spec.name = "gfsm"
  spec.version = File.read(".version")
  spec.authors = ["Zille Marco"]

  spec.summary = %q{This gem adds support to bump the semantic version bump and generate changelog file based on the commits trailer.}
  spec.description = %q{This gem adds support to bump the semantic version bump and generate changelog file based on the commits trailer.}
  spec.homepage = "https://gitlab.com/zillemarco/gitlab-flavored-semantic-versioning"
  spec.license = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 3.0.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/zillemarco/gitlab-flavored-semantic-versioning"
  spec.metadata["changelog_uri"] = "https://gitlab.com/zillemarco/gitlab-flavored-semantic-versioning/-/releases"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path("..", __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.executables = ["gfsm"]
  spec.require_paths = ["lib"]

  spec.add_dependency "yaml", "~> 0.3.0"
  spec.add_dependency "sem_version", "~> 2.0"
  spec.add_dependency "git", "~> 2.1"
end
